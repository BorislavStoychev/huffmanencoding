
public class Node {

	private int frequency;
	private char character;
	private Node left;
	private Node right;
	
	public Node(char character,int frequency,Node left,Node right)
	{
		this.character=character;
		this.frequency=frequency;
		this.left=left;
		this.right=right;
	}
	public int getFrequency()
	{
		return this.frequency;
	}
	public char getCharacter()
	{
		return this.character;
	}
	public Node getLeft()
	{
		return this.left;
	}
	public Node getRight()
	{
		return this.right;
	}
}
