import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Encode {

	private Map<Character,Integer>frequencyMap=new HashMap<>();
	private Map<Character,String>codeMap=new HashMap<>();
	private String message;
	
	public Encode(String message)
	{
		this.message=message;
		for(int i=0;i<this.message.length();i++)
		{
			//Get the symbol at position i
			char c=message.charAt(i);
			//Get its value from the map
			Integer value=frequencyMap.get(c);
			if(value!=null)
			{
				//If it already has value-increment it
				frequencyMap.put(c,++value);
			}
			else
			{
				//If it doesn't its frequency is 1
				frequencyMap.put(c, 1);
	        }
		}
		//Build the tree
		buildTree();
	}
	public int codeLength()
	{
		StringBuilder code=new StringBuilder();
		//For every symbol, append its code
		for(int i=0;i<this.message.length();i++)
		{
			code.append(codeMap.get(this.message.charAt(i)));
		}
		return code.length();
	}
	//Tree height is actually the longest road to a symbol
	//Find that symbol an return the length of its code
	public int getTreeHeight()
	{
		int temp=codeMap.get(this.message.charAt(0)).length();
		for(Map.Entry<Character, String>entry:codeMap.entrySet())
		{
			if(entry.getValue().length()>temp)
				temp=entry.getValue().length();
		}
		return temp;
	}
	private void buildTree()
	{
		PriorityQueue<Node>tree=new PriorityQueue<>(Comparator.comparing(Node::getFrequency));
		//Add all elements from frequencyMap to the tree
		for (Map.Entry<Character, Integer> entry : frequencyMap.entrySet()) 
		{
	        tree.offer(new Node(entry.getKey(),entry.getValue(), null, null));
	    }
		Node root=null;
		//Get and delete smallest two nodes and make new node with the sum of their frequencies
		//until it has only one node
		while(tree.size()>1)
		{
			Node first=tree.poll();
            Node second=tree.poll();
            Node newNode=new Node('\u0000',first.getFrequency()+second.getFrequency(),first,second);
            root=newNode;
            tree.offer(newNode);
		}
		//Make code for every symbol
		concatCode(root,"");
	}
	private void concatCode(Node root, String s) 
    { 
		if(root==null)
			return;
		//If it reaches the symbol, put the symbol and its code in codeMap
        if (root.getLeft() == null && root.getRight() == null) 
        { 
        	codeMap.put(root.getCharacter(), s);
        	return; 
        }
        concatCode(root.getLeft(), s + "0"); 
        concatCode(root.getRight(), s + "1"); 
    } 
}
